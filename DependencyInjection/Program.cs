﻿using Autofac;
using System;

namespace DependencyInjection
{
    class Program
    {
        static void Main()
        {
            try
            {
                IContainer Mappings = ConainerConf.Config();
                var MappingsScope = Mappings.BeginLifetimeScope();
                var application = MappingsScope.Resolve<IApp>();
                application.Execute().Wait();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
