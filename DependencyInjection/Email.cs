﻿namespace DependencyInjection
{
    class Email : IEmail
    {
        string _address;
        public Email()
        {
            _address = GetEmailAddress();
        }

        public string GetEmailAddress()
        {
            return "dependency@injection.com";
        }
    }
}
