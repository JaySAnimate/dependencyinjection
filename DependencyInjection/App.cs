﻿using System.Threading.Tasks;

namespace DependencyInjection
{
    class App : IApp
    {
        IBuisnessLogicToBoss _buisnessLogic;

        public App(IBuisnessLogicToBoss logic)
        {
            _buisnessLogic = logic;
        }

        public async Task Execute()
        {
            await _buisnessLogic.ProcessData();
        }
    }
}
