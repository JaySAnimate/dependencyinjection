﻿using System;
using System.Threading.Tasks;

namespace DependencyInjection
{
    class Emailer : IEmailer
    {
        IPerson _person;

        public Emailer(IPerson person)
        {
            _person = person;
        }

        public async Task SendEmail()
        {
            Console.WriteLine("Sending");
            Console.WriteLine("        Name: {0}", Person._name);
            Console.WriteLine("    LastName: {0}", Person._lastName);
            Console.WriteLine("       Email: {0}", _person.GetEmailAddress());
            for (int i = 0; i < 15; i++)
            {
                await Task.Delay(100);
                Console.Write('.');
            }
            Console.WriteLine("\nSent!");
        }
    }
}
