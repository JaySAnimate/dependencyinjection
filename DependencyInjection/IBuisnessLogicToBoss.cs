﻿using System.Threading.Tasks;

namespace DependencyInjection
{
    interface IBuisnessLogicToBoss
    {
        Task ProcessData();
    }
}