﻿namespace DependencyInjection
{
    class Person : IPerson
    {
        public static string _name;
        public static string _lastName;
        IEmail _emailAddress;
        public Person(IEmail email)
        {
            SetName();
            _emailAddress = email;
        }

        public void SetName()
        {
            _name = "James";
            _lastName = "Arthur";
        }

        public string GetEmailAddress()
        {
            return _emailAddress.GetEmailAddress();
        }
    }
}
