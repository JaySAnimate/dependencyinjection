﻿namespace DependencyInjection
{
    interface IPerson
    {
        void SetName();
        string GetEmailAddress();
    }
}