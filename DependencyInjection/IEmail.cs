﻿namespace DependencyInjection
{
    interface IEmail
    {
        string GetEmailAddress();
    }
}