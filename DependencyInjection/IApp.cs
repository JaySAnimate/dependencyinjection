﻿using System.Threading.Tasks;

namespace DependencyInjection
{
    interface IApp
    {
        Task Execute();
    }
}