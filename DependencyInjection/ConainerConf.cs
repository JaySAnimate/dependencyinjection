﻿using Autofac;

namespace DependencyInjection
{
    static class ConainerConf
    {
        public static IContainer Config()
        {
            ContainerBuilder ContBuilder = new ContainerBuilder();

            ContBuilder.RegisterType<Email>().As<IEmail>();
            ContBuilder.RegisterType<Emailer>().As<IEmailer>();
            ContBuilder.RegisterType<Person>().As<IPerson>();
            ContBuilder.RegisterType<BuisnessLogicToBoss>().As<IBuisnessLogicToBoss>();
            ContBuilder.RegisterType<App>().As<IApp>();

            return ContBuilder.Build();
        }
    }
}   
