﻿using System;
using System.Threading.Tasks;

namespace DependencyInjection
{
    class BuisnessLogicToBoss : IBuisnessLogicToBoss
    {
        IEmailer _emailer;

        public BuisnessLogicToBoss(IEmailer emailer)
        {
            _emailer = emailer;
        }

        public async Task WriteDocument()
        {
            Console.WriteLine("Writing Doc ...");
            await Task.Delay(2000);
            Console.WriteLine("Written!");
        }

        public async Task SendDocument()
        {
            await _emailer.SendEmail();
        }

        public async Task ProcessData()
        {
            await WriteDocument();
            await SendDocument();
        }
    }
}
