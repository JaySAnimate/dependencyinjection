﻿using System.Threading.Tasks;

namespace DependencyInjection
{
    interface IEmailer
    {
        Task SendEmail();
    }
}